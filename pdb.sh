for I in $(seq 0 124); do wget -nc "https://www.cia.gov/readingroom/collection/presidents-daily-brief-1961-1969?page=$I"; done
for I in $(seq 0 126); do wget -nc "https://www.cia.gov/readingroom/collection/presidents-daily-brief-1969-1977?page=$I"; done
cat presidents-daily-brief-* | grep -o "https://www.cia.gov/readingroom/docs/.*.pdf\"" | sed 's/.$//' > list.txt
wget -nc -i list.txt
