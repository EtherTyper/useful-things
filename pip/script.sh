#!/usr/bin/env bash
rm -f dl.fontawesome.com/simple
wget --recursive -nc https://dl.fontawesome.com/$TOKEN/fontawesome-pro/python/simple/

> wheels.txt
for i in $(cat packages.txt); do
    curl https://pypi.org/pypi/$i/json | python -m json.tool | grep -o "https://.*.whl" >> wheels.txt
done

for i in $(cat wheels.txt); do wget -nc $i; done
