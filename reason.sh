wget --recursive -e robots=off -nc --load-cookies $COOKIES --span-hosts --domains=reason.com,s3.amazonaws.com -I /digitaledition.reason.com/,/issue/,/archives/ https://reason.com/archives/
for i in s3.amazonaws.com/digitaledition.reason.com/*.pdf\?*; 
do
  if ! [ -L "$i" ]
  then
    mv $i "${i%%\?*}"
    ln -s "$(basename ${i%%\?*})" $i
    readlink $i
  fi
done
