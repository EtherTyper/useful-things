#!/usr/bin/env bash
for i in $(cat packages.txt); do wget --recursive --user "" --password "$TOKEN" https://npm.fontawesome.com/$i; done
cat npm.fontawesome.com/@fortawesome/* | grep -o "https://.*.tgz" > tarballs.txt
for i in $(cat tarballs.txt); do wget -nc --user "" --password "$TOKEN" $i; done
